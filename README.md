#Test task service
This page describes how to use sbt to start the test service.
It assumes you've installed sbt.

To start the service run sbt in your project directory:
```
\$ sbt server/run
```

After, to start the client for testing, run sbt in the directory of your project:
```
\$ sbt client/run
```