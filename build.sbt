import sbt.Keys.mainClass

name := "test_task_ws_root"
val akkaV = "2.5.26"
val akkaHttpV = "10.1.11"
val scalaV = "2.12.10"

resolvers += "Sonatype OSS Releases" at "http://oss.sonatype.org/content/repositories/releases/"

lazy val protocol = (project in file("protocol"))
  .settings(
    inThisBuild(List(
      organization    := "test.task",
      scalaVersion    := scalaV
    )),
    name := "protocol",
    version := "1.0",
    libraryDependencies ++= Seq(
      "io.circe" %% "circe-core" % "0.12.3",
      "io.circe" %% "circe-generic" % "0.12.3",
      "io.circe" %% "circe-parser" % "0.12.3"
    )
  )

lazy val server = (project in file("server"))
    .settings(
      inThisBuild(List(
        organization    := "test.task",
        scalaVersion    := scalaV,
        mainClass in Compile := Some("Server"),
        mainClass in assembly := Some("Server"),
        assemblyJarName in assembly := "server.jar",
      )),
      name := "server",
      version := "1.0",

      libraryDependencies ++= Seq(
        "com.typesafe.akka" %% "akka-http"   % akkaHttpV,
        "com.typesafe.akka" %% "akka-actor" % akkaV,
        "com.typesafe.akka" %% "akka-stream" % akkaV,
        "io.circe" %% "circe-core" % "0.12.3",
        "io.circe" %% "circe-generic" % "0.12.3",
        "io.circe" %% "circe-parser" % "0.12.3",
        "com.typesafe.akka" %% "akka-slf4j" % akkaV,
        "ch.qos.logback" % "logback-classic" % "1.2.3",
        "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2"
//        "ch.qos.logback" % "logback-classic" % "1.1.3" % Runtime
      )
    ).dependsOn(protocol)

lazy val client = (project in file("client"))
  .settings(
    inThisBuild(List(
      organization    := "test.task",
      scalaVersion    := scalaV,
      mainClass in Compile := Some("ClientTestApp"),
      mainClass in assembly := Some("ClientTestApp"),
      assemblyJarName in assembly := "client.jar",
    )),
    name := "client",
    version := "1.0",
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-http"   % akkaHttpV,
      "com.typesafe.akka" %% "akka-actor" % akkaV,
      "com.typesafe.akka" %% "akka-stream" % akkaV,
      "io.circe" %% "circe-core" % "0.12.3",
      "io.circe" %% "circe-generic" % "0.12.3",
      "io.circe" %% "circe-parser" % "0.12.3"
    )
  ).dependsOn(protocol)



