import java.time.Instant

import akka.actor.ActorSystem
import akka.Done
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import akka.stream.scaladsl._
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.ws._
import protocol.Request
import protocol.Request._
import protocol.Request.{AddNews, NewUser}

import scala.concurrent.duration._
import scala.concurrent.Future
import scala.language.postfixOps

object ClientTestApp extends App {
  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()
  import system.dispatcher

  def reqHelper(request: Request): TextMessage = {
    TextMessage(jsonRequestHelper(request))
  }

  val incoming: Sink[Message, Future[Done]] =
    Sink.foreach[Message] {
      case message: TextMessage.Strict =>
        println(s"incoming: ${message.text}")
    }

  case object Tick
  val outgoing = {
    Source(
      reqHelper(
        AddNews(Instant.now(), "news 1", "You must be logged in to add news.")
      ) ::
        reqHelper(NewUser("user1", "1")) ::
        reqHelper(Login("user1", "1")) ::
        reqHelper(AddNews(Instant.now(), "new 2", "News from logged in")) ::
        reqHelper(Logout) ::
        reqHelper(NewUser("user2", "1")) ::
        reqHelper(Login("user2", "1")) ::
        reqHelper(
        AddNews(Instant.now(), "new 3", "News from another logged in")
      ) ::
        reqHelper(
        AddNews(
          Instant.now(),
          "new 4",
          "Yet another News from another logged in"
        )
      ) ::
        reqHelper(FindNews(None)) ::
        reqHelper(FindNews(Some("user1"))) ::
        reqHelper(Logout) ::
        reqHelper(Login("unknown", "user")) ::
        TextMessage("") ::
        Nil
    ).throttle(1, 3 second)

  }
  outgoing.runForeach(
    ((tm: TextMessage) => println(s"Outgoing:${tm.getStrictText}"))
  )

  val webSocketFlow =
    Http().webSocketClientFlow(WebSocketRequest("ws://localhost:8080/api"))

  val (upgradeResponse, closed) =
    outgoing
      .viaMat(webSocketFlow)(Keep.right)
      .toMat(incoming)(Keep.both)
      .run()

  val connected = upgradeResponse.flatMap { upgrade =>
    if (upgrade.response.status == StatusCodes.SwitchingProtocols) {
      Future.successful(Done)
    } else {
      throw new RuntimeException(
        s"Connection failed: ${upgrade.response.status}"
      )
    }
  }

  connected.onComplete(println)
  closed.foreach(_ => println("closed"))
}
