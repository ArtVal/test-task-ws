import java.time.Instant

import io.circe.generic.auto._
import io.circe.syntax._

package object protocol {

  sealed trait Request
  object Request {
    case class NewUser(login: String, password: String) extends Request
    case class AddNews(date: Instant, title: String, body: String)
        extends Request
    case class Login(login: String, password: String) extends Request
    case class FindNews(filterByUserLogin: Option[String]) extends Request
    case object Logout extends Request

    def jsonRequestHelper(request: Request): String = {
      request.asJson.noSpaces
    }
  }

  sealed trait Response
  object Response {
    case class Error(message: String) extends Response
    case class News(date: Instant, title: String, body: String) extends Response
    case class NewsList(news: Seq[News]) extends Response

    case class NewUserSuccess(login: String) extends Response
    case object AddNewsSuccess extends Response
    case class LoginSuccess(login: String) extends Response
    case object LogoutSuccess extends Response

    def jsonResponseHelper(response: Response): String = {
      response.asJson.noSpaces
    }
  }
}
