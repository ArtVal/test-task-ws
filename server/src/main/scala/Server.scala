import akka.NotUsed
import akka.actor.{ActorRef, ActorSystem, PoisonPill}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.ws.{BinaryMessage, Message, TextMessage}
import akka.http.scaladsl.server.Directives._
import akka.stream.{ActorMaterializer, OverflowStrategy}
import akka.stream.scaladsl.{Flow, Sink, Source}
import com.typesafe.scalalogging.LazyLogging
import network.ConnectionHandlerActor
import storage.{NewsStorageActor, UserStorageActor}

import scala.concurrent.duration._
import scala.io.StdIn
import scala.language.postfixOps

object Server extends App with LazyLogging {

  implicit val system = ActorSystem("my-system")
  // needed for the future flatMap/onComplete in the end
  implicit val executionContext = system.dispatcher
  implicit val materializer = ActorMaterializer()

  val userStorage = system.actorOf(UserStorageActor.props)
  val newsStorage = system.actorOf(NewsStorageActor.props)

  userStorage ! UserStorageActor.NewUserRequest("test", "test")

  def messageHandler: Flow[Message, Message, ActorRef] = {
    val connectHandler =
      system.actorOf(ConnectionHandlerActor.props(userStorage, newsStorage))

    val incomingMessages: Sink[Message, NotUsed] =
      Flow[Message]
        .map {
          case tm: TextMessage.Strict =>
            ConnectionHandlerActor.IncomingMessage(tm)
          case bm: BinaryMessage =>
            bm.dataStream.runWith(Sink.ignore)
        }
        .to(Sink.actorRef(connectHandler, PoisonPill))

    val outgoingMessages: Source[Message, ActorRef] =
      Source
        .actorRef(100, OverflowStrategy.fail)
        .mapMaterializedValue { outActor ⇒
          connectHandler ! ConnectionHandlerActor.OutgoingActorRef(outActor)
          outActor
        }
        .map { outMsg: network.ConnectionHandlerActor.OutgoingMessage ⇒
          logger.debug(s"Outgoing: ${outMsg.message.getStrictText}")
          outMsg.message
        }

    Flow
      .fromSinkAndSourceMat(incomingMessages, outgoingMessages)(
        (_, b: ActorRef) ⇒ b
      )
      .keepAlive(30 second, () => TextMessage(""))
  }

  val route =
    path("api") {
      handleWebSocketMessages(messageHandler)
    }

  val bindingFuture = Http().bindAndHandle(route, "localhost", 8080)

  println(s"Server online at http://localhost:8080/\nPress RETURN to stop...")
  StdIn.readLine() // let it run until user presses return
  bindingFuture
    .flatMap(_.unbind()) // trigger unbinding from the port
    .onComplete(_ => system.terminate()) // and shutdown when done
}
