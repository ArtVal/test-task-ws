package logic

import akka.actor.{Actor, ActorRef, Props}
import akka.http.scaladsl.model.ws.TextMessage
import com.typesafe.scalalogging.LazyLogging
import io.circe
import io.circe.generic.auto._
import io.circe.parser.decode
import network.ConnectionHandlerActor
import network.ConnectionHandlerActor.{OutgoingMessage, outgoingMessage}
import protocol.Request
import protocol.Response.{Error, NewUserSuccess, News, _}
import storage.NewsStorageActor.{AddNewsRequest, FindNewsRequest}
import storage.UserStorageActor.{CheckUser, NewUserRequest, User}
import storage.{NewsStorageActor, UserStorageActor}

object RequestHandlerActor {

  def props(userStorage: ActorRef,
            newsStorage: ActorRef,
            connectionHandler: ActorRef): Props =
    Props(new RequestHandlerActor(userStorage, newsStorage, connectionHandler))
}

class RequestHandlerActor(userStorage: ActorRef,
                          newsStorage: ActorRef,
                          connection: ActorRef)
    extends Actor
    with LazyLogging {
  override def receive: Receive = requestHandler(None)

  def requestHandler(auth: Option[User]): Receive = {
    case UserStorageActor.NewUserSuccessResponse(login) =>
      connection ! outgoingMessage(jsonResponseHelper(NewUserSuccess(login)))
    case UserStorageActor.CheckUserResponse(userOpt: Option[User]) =>
      userOpt.fold(
        connection ! OutgoingMessage(
          TextMessage(jsonResponseHelper(Error("Bad credentials")))
        )
      ) { user =>
        connection ! outgoingMessage(
          jsonResponseHelper(LoginSuccess(user.login))
        )
        context.become(requestHandler(Some(user)))
      }
    case UserStorageActor.NewUserFailureResponse(login, message) =>
      connection ! outgoingMessage(
        jsonResponseHelper(Error(s"Registration error for $login. $message"))
      )
    case NewsStorageActor.AddNewsSuccess =>
      connection ! outgoingMessage(jsonResponseHelper(AddNewsSuccess))
    case NewsStorageActor.NewsListResponse(news) =>
      connection ! outgoingMessage(jsonResponseHelper(NewsList(news)))
    case ConnectionHandlerActor.IncomingMessage(incoming: TextMessage.Strict) =>
      if (!incoming.text.isEmpty) {
        logger.debug(s"Incoming: ${incoming.text}")
        decode[Request](incoming.text) match {
          case Left(e: circe.Error) =>
            connection ! outgoingMessage(
              jsonResponseHelper(Error(e.getMessage))
            )
          case Right(value: Request) =>
            value match {
              case Request.NewUser(login, password) =>
                logger.debug(userStorage.path.name)
                userStorage ! NewUserRequest(login, password)
              case Request.Login(login, password) =>
                userStorage ! CheckUser(login, password)
              case Request.AddNews(date, title, body) =>
                auth.fold {
                  logger.debug(s"conn: $connection")
                  connection ! outgoingMessage(
                    jsonResponseHelper(Error("Please login"))
                  )
                } { u: User =>
                  newsStorage ! AddNewsRequest(News(date, title, body), u.login)
                }
              case Request.FindNews(filterByUserLogin) =>
                newsStorage ! FindNewsRequest(filterByUserLogin)
              case Request.Logout =>
                connection ! outgoingMessage(jsonResponseHelper(LogoutSuccess))
                context.become(requestHandler(None))
            }
        }
      }
  }
}
