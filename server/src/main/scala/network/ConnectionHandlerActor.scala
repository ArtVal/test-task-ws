package network

import akka.actor.{Actor, ActorRef, PoisonPill, Props, Terminated}
import akka.http.scaladsl.model.ws.TextMessage
import akka.stream.ActorMaterializer
import network.ConnectionHandlerActor.{
  IncomingMessage,
  OutgoingActorRef,
  OutgoingMessage
}
import com.typesafe.scalalogging.LazyLogging
import jdk.nashorn.internal.ir.RuntimeNode.Request
import logic.RequestHandlerActor

import scala.concurrent.ExecutionContext

object ConnectionHandlerActor {
  sealed trait Connection
  case class IncomingMessage(message: TextMessage) extends Connection
  case class OutgoingMessage(message: TextMessage) extends Connection
  case class OutgoingActorRef(outgoing: ActorRef) extends Connection
  case object Stop extends Connection

  def outgoingMessage(message: String): OutgoingMessage =
    OutgoingMessage(TextMessage(message))

  def props(userStorage: ActorRef, newsStorage: ActorRef) =
    Props(new ConnectionHandlerActor(userStorage, newsStorage))

}

class ConnectionHandlerActor(userStorage: ActorRef, newsStorage: ActorRef)
    extends Actor
    with LazyLogging {
  val requestHandler: ActorRef = context.system.actorOf(
    RequestHandlerActor.props(userStorage, newsStorage, self)
  )
  override def receive: Receive = idle()

  private def idle(): Receive = {
    case OutgoingActorRef(outgoing) =>
      context.watch(outgoing)
      context.become(active(outgoing))
    case outMsg: OutgoingMessage =>
      logger.debug(outMsg.toString)
    case _ => logger.debug(s"Unknown message")
  }

  private def active(outgoing: ActorRef): Receive = {
    case msg: IncomingMessage =>
      requestHandler ! msg
    case msg: OutgoingMessage =>
      outgoing ! msg
    case Terminated(_) ⇒
      logger.debug(s"Connection: $outgoing. Terminated")
      context.stop(self)
    case PoisonPill =>
      logger.debug(s"Poison Pill")
      context.stop(self)
    case _ => logger.error(s"Unknown message")
  }

  override def postStop(): Unit = {
    logger.debug(s"Connection. Handler stopped")
    requestHandler ! PoisonPill
    super.postStop()
  }
}
