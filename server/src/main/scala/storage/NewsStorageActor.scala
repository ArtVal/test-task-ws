package storage

import akka.actor.{Actor, Props}
import com.typesafe.scalalogging.LazyLogging
import protocol.Response.News
import storage.NewsStorageActor.{
  AddNewsRequest,
  AddNewsSuccess,
  FindNewsRequest,
  NewsListResponse,
  UserNews
}

object NewsStorageActor {
  case class FindNewsRequest(filterByName: Option[String])
  case class AddNewsRequest(news: News, login: String)
  case class UserNews(news: News, login: String)

  case object AddNewsSuccess
  case class NewsListResponse(news: Seq[News])

  def props = Props[NewsStorageActor]
}
class NewsStorageActor extends Actor with LazyLogging {

  override def receive: Receive = newsStorage(Seq.empty)

  def newsStorage(userNews: Seq[UserNews]): Receive = {
    case FindNewsRequest(filterByName) =>
      sender() ! NewsListResponse(
        filterByName
          .fold(userNews) { filter =>
            userNews.filter(_.login == filter)
          }
          .map(_.news)
      )
    case AddNewsRequest(news, login) =>
      sender() ! AddNewsSuccess
      context.become(newsStorage(userNews :+ UserNews(news, login)))
    case u => logger.debug(s"unknown message $u")
  }

}
