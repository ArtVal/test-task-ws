package storage

import akka.actor.{Actor, Props}
import com.typesafe.scalalogging.LazyLogging
import storage.UserStorageActor.{
  CheckUser,
  CheckUserResponse,
  NewUserFailureResponse,
  NewUserRequest,
  NewUserSuccessResponse,
  User
}

object UserStorageActor {
  case class User(login: String, password: String)
  case class NewUserRequest(login: String, password: String)
  case class CheckUser(login: String, password: String)

  case class CheckUserResponse(user: Option[User])
  case class NewUserSuccessResponse(login: String)
  case class NewUserFailureResponse(login: String, message: String)

  def props: Props = Props[UserStorageActor]
}
class UserStorageActor extends Actor with LazyLogging {

  logger.debug("Start UserStorage")

  override def receive: Receive = userStorage(Seq.empty[User])

  private def userStorage(users: Seq[User]): Receive = {
    case NewUserRequest(login, password) =>
      if (!users.exists(_.login == login)) {
        context.sender() ! NewUserSuccessResponse(login)
        context.become(userStorage(users :+ User(login, password)))
      } else {
        context.sender() ! NewUserFailureResponse(login, "User already exists")
      }
    case CheckUser(login, password) =>
      val result: Option[User] =
        users.find(p => p.login == login && p.password == password)
      context.sender() ! CheckUserResponse(result)
    case k => logger.debug(s"Unknown message $k")
  }
}
